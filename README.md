```
██╗   ██╗ ██████╗ ██╗   ██╗██████╗ ██╗     
╚██╗ ██╔╝██╔═══██╗██║   ██║██╔══██╗██║     
 ╚████╔╝ ██║   ██║██║   ██║██████╔╝██║     
  ╚██╔╝  ██║   ██║██║   ██║██╔══██╗██║     
   ██║   ╚██████╔╝╚██████╔╝██║  ██║███████╗
   ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚══════╝  
```
# URL shortner and upload files direct from terminal
Send files direct from terminal to https://0x0.st or short url by https://tinyurl.com

Autor <a href="https://www.linkedin.com/in/matheushsmartins/">Matheus Martins<a>

Works on 
- GNU/Linux 
- MacOS

Depends: 
- curl 
- xclip
- pbcopy(MacOS)

Usage:
To short links:
```
yourl.sh 'http://www.urtoshortner.com'
```

To sent files:
```
yourl.sh '/Path/To/Your/File'
```

### Disclamer
Maximum file size: 512.0 MiB
Blocked file types: application/x-dosexec, application/x-executable, application/x-hdf5, application/java-archive, Android APKs and system images.

TERMS OF SERVICE
----------------
This is NOT a platform for:
- piracy
- pornography and gore
- extremist material of any kind
- malware / botnet C&C
- anything related to crypto currencies
- backups (yes, this includes your minecraft stuff, seriously
    people have been dumping terabytes of it here for years)
- CI build artifacts
- doxxing, database dumps containing personal information
- anything illegal under German law
